# Coffee Shop POS System
## Table of contents

- [Overview](#overview)
  - [Screenshot](#screenshot)
  - [Links](#links)
  - [Built with](#built-with)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)


## Overview

- this is a point of sale system that will allow cashiers to take coffee orders from customers with their names, allow baristas to log into the app and see orders that have been made, and mark them as complete. Orders that have been completed will note which barista completed the order. 
### Screenshot

![](./images/desktop-version.png)
![](./images/mobile-version.png)

### Links

- GIthub URL: [https://github.com/Asantae/hip-coffee-co]
- Live Site URL: []

### Built with

- Semantic HTML5 markup
- CSS3
- Javascript
- Node.js
- MongoDb
- ejs

### Continued development

```
may add more features later such as customer self-serve options or guest log in (with limited actions)
```
### Useful resources

- [Stack Overflow](https://stackoverflow.com/) - A lot of the solutions that I couldn't figure out on my own were from this site. It also provided inspiration a lot of times for different js solutions/error handling
- [Npm JS Docs](https://www.npmjs.com/package/cookie-parser) - I had to refer to the npm documentation for node packages I was not very familiar with

## Author

- linkedIn - [@AsantaeShelton](https://www.linkedin.com/in/asantae-shelton-572467229/)
- Twitter - [@AsantaeMS](https://www.twitter.com/AsantaeMS)
- Github - [@Asantae](https://www.github.com/Asantae)