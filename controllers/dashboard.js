const User = require("../models/user");

module.exports = {
  getIndex: (req, res) => {
    res.render("dashboard.ejs");
  },
  getUser: async (req, res) => {
    try {
      const token = getCookie(req.headers.cookie).token;
      const user = await User.findOne({ token }).lean()
      if(user.admin){
        res.render("dashboard.ejs", { user: user.username, role: 'Admin' })
      } else {
        res.render("dashboard.ejs", { user: user.username, role: user.role })
      }
    } catch (err) {
      console.log(err)
      res.redirect("/login")
    }
  },

};

function getCookie(token){
  let x = token.split('=')[1].split(' ')[0].split(',').map(x => x.replaceAll('"', '').replaceAll('{','').replaceAll('}','').replaceAll(';','')).map(x => x.split(':'))
  x = Object.fromEntries(x)
  return x
}