const { db } = require('../models/item')
const Item = require("../models/item");
const User = require("../models/user");
module.exports = {
  viewItems: async (req, res) => {
    try {
      const token = getCookie(req.headers.cookie).token;
      const user = await User.findOne({ token }).lean()
      const items = await db.collection('item').find().toArray()
      if(user.admin){
        res.render("view-menu.ejs", { items: items, user: user.username, role: 'Admin' })
      } else {
        res.render("view-menu.ejs", { items: items, user: user.username, role: user.role })
      }
    } catch (err) {
      console.log(err);
    }
  },
  addItem: async (req, res) => {
    const { creator, name, categoryChoice: category, itemPrice: price, ingredients } = req.body
    try {
      const response = await Item.create({
        creator, 
        name, 
        category, 
        price, 
        ingredients
      })
      return res.json({ status: 'ok', response })
    } catch (error) {
      console.log(error)
      if(error.code === 11000){
        return res.json({status: 'error', error: 'This item-name already exists'})
      }
    }
  },
  deleteItem: async (req, res) => {
    const { itemName } = req.body
    await db.collection('item').deleteOne({ name: itemName })
    .then(result => {
        res.json('item Deleted')
    })
    .catch(error => console.error(error))
  }

}

function getCookie(token){
  let x = token.split('=')[1].split(' ')[0].split(',').map(x => x.replaceAll('"', '').replaceAll('{','').replaceAll('}','').replaceAll(';','')).map(x => x.split(':'))
  x = Object.fromEntries(x)
  return x
}