const { db } = require('../models/item')
const User = require("../models/user");
module.exports = {
  getItems: async (req, res) => {
    try {
      const token = getCookie(req.headers.cookie).token;
      const user = await User.findOne({ token }).lean()
      const items = await db.collection('item').find().toArray()
      if(user.admin){
        res.render("orders.ejs", { items: items, user: user.username, role: 'Admin' })
      } else {
        res.render("orders.ejs", { items: items, user: user.username, role: user.role })
      }
    } catch (err) {
      console.log(err);
    }
  },
  postItems: async (req, res) => {
    const token = getCookie(req.headers.cookie).token;
    const user = await User.findOne({ token }).lean()
  }
}

function getCookie(token){
  let x = token.split('=')[1].split(' ')[0].split(',').map(x => x.replaceAll('"', '').replaceAll('{','').replaceAll('}','').replaceAll(';','')).map(x => x.split(':'))
  x = Object.fromEntries(x)
  return x
}