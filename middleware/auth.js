const jwt = require("jsonwebtoken")
const env = require('dotenv').config( {path: "./config/.env"} )
const User = require("../models/user");

module.exports = {
  ensureAuth: async function (req, res, next) {
    // Verify the token using jwt.verify method
    try {
      const token = getCookie(req.headers.cookie).token;
      jwt.verify(token, process.env.JWT_SECRET);
      return next();
    } catch(err){
      console.log('error: ' + err.name);
      res.redirect("/login")   ;
    }
  },
  ensureAdmin: async function (req, res, next) {
    const token = getCookie(req.headers.cookie).token;
    const user = await User.findOne({ token }).lean()

    //Verify if user is an admin
    if(user.admin){
      return next()
    } else {
      alert('Administrator access is needed. Returning to login page')
      res.redirect('/login')
    }
  }
};

function getCookie(token){
  let x = token.split('=')[1].split(' ')[0].split(',').map(x => x.replaceAll('"', '').replaceAll('{','').replaceAll('}','').replaceAll(';','')).map(x => x.split(':'))
  x = Object.fromEntries(x)
  return x
}

function deleteCookie(){
  let y = document.cookie + ';'
  document.cookie = y + 'expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;'                                                                                                                            
}