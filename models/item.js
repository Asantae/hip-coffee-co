const mongoose = require('mongoose')

const ItemSchema = new mongoose.Schema(
    {
    creator: { type: String, required: true, unique: false },
    name: { type: String, required: true, unique: true },
    category: { type: String, required: true },
    price: { type: Number, required: true },
    ingredients: { type: String, default: 'none' }
    },
    { collection: 'item'}
)

const model = mongoose.model('ItemSchema', ItemSchema)

module.exports = model