const clockIn = document.querySelector("#clock-in")
const logOut = document.querySelector("#log-out")
const viewMenu = document.querySelector("#view-menu")
const editMenu = document.querySelector("#edit-items")

//navigation buttons
viewMenu.addEventListener('click', () => {
    window.location.href = 'view-menu'
})
logOut.addEventListener('click', () => {
    deleteCookie()
    window.location.href = 'login'
})
clockIn.addEventListener('click', () => {
    window.location.href = 'orders'
})


