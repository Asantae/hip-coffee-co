//this button return user to dashboard.ejs
const backButton = document.querySelector('#go-back-button')
backButton.addEventListener('click', () => {
    window.location.href='dashboard'
})

const deleteText = document.querySelectorAll('.fa-trash')
Array.from(deleteText).forEach((element)=>{
    element.addEventListener('click', deleteItem)
})

const itemForm = document.querySelector('#item-form');

itemForm.addEventListener('submit', submitItem)

async function deleteItem(){
    console.log(this.parentNode.childNodes[0].nextElementSibling.innerText)
    const itemName = this.parentNode.childNodes[0].nextElementSibling.innerText
    try{
        const response = await fetch('/deleteItem', {
            method: 'delete',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
              itemName
            })
        })
        const data = await response.json()
        console.log(data)
        location.reload()

    }catch(error){
        console.log('error')
    }
}

//changes cookie information into an object
function getCookie(){
    let x = document.cookie.split('=')[1].split(' ')[0].split(',').map(x => x.replaceAll('"', '').replaceAll('{','').replaceAll('}','').replaceAll(';','')).map(x => x.split(':'))
    x = Object.fromEntries(x)
    return x
}

//resets the cookies on the page
function deleteCookie(){
    let y = document.cookie + ';'
    document.cookie = y + 'expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;'                                                                                                                            
}

//controls what happens when an item is submitted
async function submitItem(event){
    event.preventDefault()
    x = getCookie()
    const creator = x.user
    const name = document.getElementById('item-name').value
    const itemPrice = parseFloat(document.getElementById('new-item-price').value)
    const ingredients = document.getElementById('new-item-ingredients').value
    const category = document.getElementById('new-category')
    const categoryChoice = category.options[category.selectedIndex].value

    const result = await fetch("/create", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            creator,
            name,
            categoryChoice,
            itemPrice,
            ingredients
        })
    }).then((res) => res.json())
    if (result.status === 'ok') {
        console.log('new item created ' + result.status)
        location.reload()
    } else {
        alert('something went wrong ' + result.error)
    }
}