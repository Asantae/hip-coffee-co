const employee = document.getElementById('employee-id')
const buttonDown = document.getElementById('slideDown');
const buttonUp = document.getElementById('slideUp');

buttonDown.onclick = function () {
    document.getElementById('receipt-scroll-button').scrollTop -= 100;
};
buttonUp.onclick = function () {
    document.getElementById('receipt-scroll-button').scrollTop += 100;
};

function setCookie(x){
    const d = new Date()
    d.setTime(d.getTime() + (12*60*60*1000))
    let expires = 'expires='+d.toUTCString
    document.cookie = 'jsonCookie=' + x + ';' + expires + ';path=/'
}


//below controls the button functionality for the food items rendered from the database
const overlayOne = document.querySelector('#overlay-top')
const drinkOverlay = document.querySelector('#drink-overlay')
const breakfastOverlay = document.querySelector('#breakfast-overlay')
const bakeryOverlay = document.querySelector('#bakery-overlay')
const specialsOverlay = document.querySelector('#specials-overlay')

const drinkButton = document.querySelector('#drinks-category')
drinkButton.addEventListener('click', () => {
    overlayOne.classList.add('show')
    drinkOverlay.classList.remove('hide')
    drinkOverlay.classList.add('show')
})

const breakfastButton = document.querySelector('#breakfast-category')
breakfastButton.addEventListener('click', () => {
    overlayOne.classList.add('show')
    breakfastOverlay.classList.remove('hide')
    breakfastOverlay.classList.add('show')
})

const bakeryButton = document.querySelector('#bakery-category')
bakeryButton.addEventListener('click', () => {
    overlayOne.classList.add('show')
    bakeryOverlay.classList.remove('hide')
    bakeryOverlay.classList.add('show')
})

const specialsButton = document.querySelector('#specials-category')
specialsButton.addEventListener('click', () => {
    overlayOne.classList.add('show')
    specialsOverlay.classList.remove('hide')
    specialsOverlay.classList.add('show')
})

const exitButton = document.querySelector('#back-button')
exitButton.addEventListener('click', () => {
    overlayOne.classList.remove('show')
    drinkOverlay.classList.remove('show')
    breakfastOverlay.classList.remove('show')
    bakeryOverlay.classList.remove('show')
    specialsOverlay.classList.remove('show')
    drinkOverlay.classList.add('hide')
    breakfastOverlay.classList.add('hide')
    bakeryOverlay.classList.add('hide')
    specialsOverlay.classList.add('hide')
})

//this will begin orders and add them to the receipt information for the baristas
const orderContainer = document.getElementById('new-orders')
const orderButton = document.querySelector('.add-order')
const nameModal = document.querySelector(".enter-name-modal")
const customerNameForm = document.querySelector(".customer-order-form")
const customerNameInput = document.querySelector(".name-enter-input")


//adds customer name to the 'order' section of the page
function addCustomerName(event){
    event.preventDefault()
    const customerName = document.querySelectorAll('.customer-name')
    customerName[customerName.length - 1].innerHTML = customerNameInput.value
    const orders = getOrders()
    const targetOrder = orders.filter(order => order.status == 'Active')[0];
    
    targetOrder.content.customerName = customerNameInput.value
    console.log(customerNameInput.value)
    console.log(targetOrder.content.customerName)
    customerNameInput.value = ''
    nameModal.classList.remove('show')
    saveOrder(orders)
}

getOrders().forEach(order => {
    const orderElement = createOrderElement(order.id, order.content)
    orderContainer.appendChild(orderElement)
})

customerNameForm.addEventListener('submit', () => addCustomerName(event))
orderButton.addEventListener('click', () => addOrder())

//adds the "active" status to an order
function setActiveOrder(id){
    const orders = getOrders()
    orders.forEach(order => { order.status = 'Inactive' })
    const targetOrder = orders.filter(order => order.id == id)[0]
    targetOrder.status = 'Active'
    saveOrder(orders)
}

//retrieves existing orders from the local storage in the clients browser
function getOrders(){
    return JSON.parse(localStorage.getItem("neworders-orders") || "[]")
}

//saves new orders to local storage
function saveOrder(orders){
    localStorage.setItem("neworders-orders", JSON.stringify(orders))
}

//builds a new HTML element that represents an order
function createOrderElement(id, content){
    const element = document.createElement('div')
    element.classList.add('order-box')
    
    
    const elementChildSpan = document.createElement('span')
    elementChildSpan.classList.add('customer-name')
    element.appendChild(elementChildSpan)
    elementChildSpan.innerHTML = content.customerName
    
    const elementChildOutput = document.createElement('output')
    elementChildOutput.classList.add('order-output')
    element.appendChild(elementChildOutput)

    const elementOutputChild1 = document.createElement('div')
    const elementOutputChild2 = document.createElement('div')
    elementOutputChild1.classList.add('order-item')
    elementOutputChild2.classList.add('order-price')
    elementChildOutput.appendChild(elementOutputChild1)
    elementChildOutput.appendChild(elementOutputChild2)
    elementOutputChild1.innerHTML = content.orderContent
    elementOutputChild2.innerHTML = content.orderContent
    elementChildOutput.addEventListener('change', () => {
        updateOrder(id, elementChildSpan, elementChildOutput)
    })
    
    elementChildSpan.addEventListener('change', () => {
        updateOrder(id, elementChildSpan, elementChildOutput)
    })

    element.addEventListener('dblclick', () => {
        const doDelete = confirm('are you sure')
        if(doDelete){
            removeOrder(id, element)
        }
    })

    element.addEventListener('click', () => setActiveOrder(id))

    return element
}

//adds a new order to the HTML and saves it to local storage
function addOrder(){

    const orders = getOrders()
    orders.forEach(orders => { orders.status = 'Inactive' })
    const orderObject = {
        id: Math.floor(Math.random() * 100000),
        content: { customerName: 'Empty', orderContent: 'Empty' },
        status: 'Active'
    }

    const orderElement = createOrderElement(orderObject.id, orderObject.content)

    orders.push(orderObject)
    orderContainer.appendChild(orderElement)
    saveOrder(orders)
    nameModal.classList.add('show')
}

//updates orders on the page and in the localstorage
function updateOrder(id, newContentName, newContentOrder){
    const orders = getOrders();
    const targetOrder = orders.filter(order => order.id == id)[0];
    console.log(newContentName)
    targetOrder.content.customerName = newContentName
    targetOrder.content.orderContent = newContentOrder
    saveOrder(orders)
}

//removes the HTML element representing the targeted order
function removeOrder(id, element){
    const orders = getOrders().filter(order => order.id != id)

    saveOrder(orders)
    orderContainer.removeChild(element)
    
}

//finds the order currently being worked on and created new html elements within it
function findActiveOrder(Name, Price){
    const orders = getOrders();
    const targetOrder = orders.filter(order => order.status == 'Active')[0];
    
    
}

//adds order item to active order, or creates a new order is there is none
const drinkChoice = document.querySelectorAll('.drinks-item-button')
for(let i=0; i < drinkChoice.length; i++){
    drinkChoice[i].addEventListener("click", event => {
        itemChoice(event)
    })
}

const breakfastChoice = document.querySelectorAll('.breakfast-item-button')
for(let i=0; i < breakfastChoice.length; i++){
    breakfastChoice[i].addEventListener("click", event => {
        itemChoice(event)
    })
}

const bakeryChoice = document.querySelectorAll('.bakery-item-button')
for(let i=0; i < bakeryChoice.length; i++){
    bakeryChoice[i].addEventListener("click", event => {
        itemChoice(event)
    })
}

const specialsChoice = document.querySelectorAll('.specials-item-button')
for(let i=0; i < specialsChoice.length; i++){
    specialsChoice[i].addEventListener("click", event => {
        itemChoice(event)
    })
}

function itemChoice(event){
    let itemName = event.currentTarget.childNodes[1].childNodes[1].innerText
    let itemPrice = event.currentTarget.childNodes[1].childNodes[3].innerText.split('$')[1]
    console.log(event.currentTarget.childNodes[1].childNodes[1].innerText)
    console.log(event.currentTarget.childNodes[1].childNodes[3].innerText.split('$')[1]) 
    findActiveOrder(itemName, itemPrice) 
    
}

//gets the current time and displays it to the user
function currentTime() {
  let date = new Date(); 
  let hh = date.getHours();
  let mm = date.getMinutes();
  let ss = date.getSeconds();
  let session = "AM";
  if(hh === 0){
      hh = 12;
  }
  if(hh > 12){
      hh = hh - 12;
      session = "PM";
   }
   hh = (hh < 10) ? "0" + hh : hh;
   mm = (mm < 10) ? "0" + mm : mm;
   ss = (ss < 10) ? "0" + ss : ss;
   let time = hh + ":" + mm + ":" + ss + " " + session;
  document.getElementById("clock").innerText = time; 
  let t = setTimeout(function(){ currentTime() }, 1000);
}

window.onload = currentTime();