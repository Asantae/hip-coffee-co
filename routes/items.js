const express = require("express");
const router = express.Router();
const menuController = require("../controllers/menu");
const { ensureAuth, ensureAdmin } = require("../middleware/auth");


router.post("/create", ensureAuth, ensureAdmin, menuController.addItem)

router.delete("/deleteItem", ensureAuth, ensureAdmin, menuController.deleteItem)
module.exports = router;