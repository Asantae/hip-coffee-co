const express = require("express");
const router = express.Router();
const authController = require("../controllers/auth");
const dashboardController = require("../controllers/dashboard");
const menuController = require("../controllers/menu");
const ordersController = require("../controllers/orders");
const { ensureAuth, ensureAdmin } = require("../middleware/auth");

router.get("/", dashboardController.getIndex);
router.get('/dashboard', ensureAuth, dashboardController.getUser);
router.get("/view-menu", ensureAuth, menuController.viewItems);
router.get('/orders', ensureAuth, ordersController.getItems);
router.post('/orders', ensureAuth, ordersController.postItems);
router.get("/login", authController.getLogin);
router.post("/login", authController.postLogin);
router.get("/logout", authController.logout);
router.get("/register", authController.getRegister);
router.post("/register", authController.postRegister);
module.exports = router;
