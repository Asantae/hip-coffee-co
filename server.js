const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const connectDB = require("./config/database");
const mainRoutes = require("./routes/main");
const itemRoutes = require("./routes/items")

//Use .env file in config folder
const env = require('dotenv').config( {path: "./config/.env"} )

//Connect to database
connectDB();

app.set('views' + __dirname + '/views')
app.set('view engine', 'ejs');
app.use(bodyParser.json())
app.use(express.static(__dirname + '/public'))
app.use("/", mainRoutes);
app.use("/items", itemRoutes);

//server is running
app.listen(process.env.PORT, () => {
    console.log(`listening on port ${process.env.PORT}`)
})